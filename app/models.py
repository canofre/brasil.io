from . import db, app


class Municipio(db.Model):
    __tablename__ = 'tb_municipios'
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(64))
    codigo_ibge = db.Column(db.Integer)
    url = db.Column(db.Text)

    def __repr__(self):
        return '<Municipio %r>' % (self.nome)
