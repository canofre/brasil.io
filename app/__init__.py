from flask import Flask
from flask_sqlalchemy import SQLAlchemy
# from whoosh.analysis import StemmingAnalyzer
# from flask_moment import Moment
# from flask_debugtoolbar import DebugToolbarExtension
from config import config


db = SQLAlchemy()

# moment = Moment()
# toolbar = DebugToolbarExtension()
app = Flask(__name__)


def create_app(config_name):

    # app.config['WHOOSH_ANALYZER'] = StemmingAnalyzer()
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    db.init_app(app)
    # toolbar.init_app(app)
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
