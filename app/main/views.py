from flask import render_template, request, url_for, redirect
from .forms import SearchForm
from . import main
from .. import db
from ..models import Municipio
import requests
import json
from sqlalchemy import or_


@main.route('/atualizar')
def atualizar():
    r = requests.get('https://api.brasil.io/mg/', verify=False)
    data_json = json.loads(r.content.decode('utf-8'))

    for data in data_json['municipios']:
        municipio = Municipio.query.filter_by(nome=data['nome']).first()
        if municipio is None:
            m = Municipio()
            m.nome = data['nome']
            m.url = data['url']
            m.codigo_ibge = data['codigo-ibge']
            db.session.add(m)
    db.session.commit()
    return redirect(url_for('.index'))


@main.route('/', methods=['GET', 'POST'])
def index():
    search = SearchForm()
    page = request.args.get('page', 1, type=int)
    pagination = Municipio.query.order_by('nome').paginate(page, per_page=10, error_out=False)
    municipios = pagination.items
    return render_template('index.html', form=municipios, pagination=pagination, formSearch=search)


@main.route('/search', methods=['POST'])
def search():
    form = SearchForm()
    if form.validate_on_submit():
        return redirect(url_for('main.search_results', query=form.search.data))
    return redirect(url_for('index'))


@main.route('/search_results/<query>')
def search_results(query):
    search = SearchForm()
    page = request.args.get('page', 1, type=int)
    pagination = Municipio.query.filter(or_(Municipio.nome.like("%"+query+"%"), Municipio.codigo_ibge.like("%"+query+"%"))).paginate(page, per_page=10, error_out=False)
    municipios = pagination.items
    return render_template('search.html',
                           query=query,
                           form=municipios,
                           pagination=pagination,
                           formSearch=search)
