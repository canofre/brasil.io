Brasil.io
=========
Sistemas para busca de cidades por nome e codigo do ibge.

instalção
---------
Sistema feito com Python 3, flask, bootstrap e virtualenv

criar a variavel de ambiente SECRET_KEY

```
  git clone git@bitbucket.org:canofre/brasil.io.git

  cd brasil.io
  virtualenv --python=python3 venv
  source venv/bin/activate
  sudo pip install -r requeriments.txt

  python manage.py db init
  python manage.py db migrate
  python manage.py db upgrade

  python manage.py runserver
```
